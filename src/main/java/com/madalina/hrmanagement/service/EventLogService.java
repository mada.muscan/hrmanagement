package com.madalina.hrmanagement.service;

import com.madalina.hrmanagement.entity.EventLogEntity;

import java.util.List;

public interface EventLogService {
    EventLogEntity save(EventLogEntity entity);

    EventLogEntity readById(Long id);

    List<EventLogEntity> readAll(Integer pageNumber, Integer pageSize);

    void delete(Long id);
}
