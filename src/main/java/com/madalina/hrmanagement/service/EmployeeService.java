package com.madalina.hrmanagement.service;

import com.madalina.hrmanagement.dto.FormWrapper;
import com.madalina.hrmanagement.entity.EmployeeEntity;
import com.madalina.hrmanagement.service.exception.NoEntityWithSuchIdException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeService {

    EmployeeEntity save(EmployeeEntity entity);

    EmployeeEntity readById(Long id);

    List<EmployeeEntity> readAll(Integer pageNumber, Integer pageSize);

    void delete(Long id);

    Integer countEmployeesByCompanyId(Long id);

    List<EmployeeEntity> getEmployeesByCompanyId(Long id, int page, int size);

    EmployeeEntity createEmployee(FormWrapper formWrapper);

     List<Integer> getPagesEmployeesByCompanyId(Long companyId, Integer pageSize);

    List<Integer> getPagesEmployees(int pageSize);

}