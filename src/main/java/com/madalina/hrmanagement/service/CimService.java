package com.madalina.hrmanagement.service;

import com.madalina.hrmanagement.entity.CimEntity;

import java.util.List;

public interface CimService {
    CimEntity save(CimEntity entity);

    CimEntity readById(Long id);

    List<CimEntity> readAll(Integer pageNumber, Integer pageSize);

    void delete(Long id);

    CimEntity readByEmployeeId(Long employeeId);
}
