package com.madalina.hrmanagement.service.exception;

public class NoEntityWithSuchIdException extends RuntimeException {

    public NoEntityWithSuchIdException() {
        super();
    }

    public NoEntityWithSuchIdException(String message) {
        super(message);
    }

    public NoEntityWithSuchIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoEntityWithSuchIdException(Throwable cause) {
        super(cause);
    }
}
