package com.madalina.hrmanagement.service;

import com.madalina.hrmanagement.entity.MedicalCheckEntity;

import java.util.List;

public interface MedicalCheckService {
    MedicalCheckEntity save(MedicalCheckEntity entity);

    MedicalCheckEntity readById(Long id);

    List<MedicalCheckEntity> readAll(Integer pageNumber, Integer pageSize);

    void delete(Long id);
}
