package com.madalina.hrmanagement.service.impl;

import com.madalina.hrmanagement.dao.CimDao;
import com.madalina.hrmanagement.dao.CompanyDao;
import com.madalina.hrmanagement.dao.EmployeeDao;
import com.madalina.hrmanagement.dto.FormWrapper;
import com.madalina.hrmanagement.entity.CimEntity;
import com.madalina.hrmanagement.entity.CompanyEntity;
import com.madalina.hrmanagement.entity.EmployeeEntity;
import com.madalina.hrmanagement.service.EmployeeService;
import com.madalina.hrmanagement.service.exception.NoEntityWithSuchIdException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private CimDao cimDao;

    @Override
    public EmployeeEntity save(EmployeeEntity entity) {
        return employeeDao.saveAndFlush(entity);
    }

    @Override
    public EmployeeEntity readById(Long id) {
        return employeeDao.findById(id)
                .orElseThrow(() -> new NoEntityWithSuchIdException("No entity with such id"));
    }

    @Override
    public List<EmployeeEntity> readAll(Integer pageNumber, Integer pageSize) {
        Page<EmployeeEntity> resultPage = employeeDao.findAll(PageRequest.of(pageNumber, pageSize));
        return resultPage.toList();
    }

    @Override
    public void delete(Long id) {
        employeeDao.deleteById(id);
    }

    @Override
    public Integer countEmployeesByCompanyId(Long id) {
        return employeeDao.countEmployeesByCompanyId(id);
    }

    @Override
    public List<Integer> getPagesEmployees(int pageSize) {
        Integer count = employeeDao.countEmployees();

        int numberOfPages = count / pageSize;
        if(count % pageSize > 0){
            numberOfPages++;
        }
        List<Integer> pages = new ArrayList<>();
        for (int  i =0; i<numberOfPages; i++ ){
            pages.add(i+1);
        }
        return pages;
    }

    @Override
    public List<Integer> getPagesEmployeesByCompanyId(Long companyId, Integer pageSize){
        Integer count = this.countEmployeesByCompanyId(companyId);
        int numberOfPages = count / pageSize;
        if(count % pageSize > 0){
            numberOfPages++;
        }
        List<Integer> pages = new ArrayList<>();
        for (int  i =0; i<numberOfPages; i++ ){
            pages.add(i+1);
        }
       return pages;
    }

    @Override
    public List<EmployeeEntity> getEmployeesByCompanyId(Long id, int page, int size) {
        Pageable of = PageRequest.of(page, size);
        return employeeDao.getEmployeesByCompanyId(id,of).toList();
    }

    public EmployeeEntity createEmployee(FormWrapper formWrapper) {
        CimEntity cimEntity = new CimEntity();
        String inputCim = formWrapper.getInputCim();
        cimEntity.setNumber(inputCim);
        cimEntity.setStartDate(LocalDateTime.now());
        cimEntity.setExpirationDate(LocalDateTime.now().plusYears(2));

        String inputFirstName = formWrapper.getInputFirstName();
        String inputLastName = formWrapper.getInputLastName();
        String inputPhoneNumber = formWrapper.getInputPhoneNumber();
        String inputOccupation = formWrapper.getInputOccupation();

        Set<CompanyEntity> companyEntities = new HashSet<>();
        CompanyEntity companyEntity = companyDao.findById(formWrapper.getInputCompany()).orElseThrow(() -> new NoEntityWithSuchIdException("NO COMPANY WITH SUCH ID"));
        companyEntities.add(companyEntity);

        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setFirstName(inputFirstName);
        employeeEntity.setLastName(inputLastName);
        employeeEntity.setTelephone(inputPhoneNumber);
        employeeEntity.setOccupation(inputOccupation);
        employeeEntity.setCompanies(companyEntities);
        employeeEntity.setActive(formWrapper.getIsActive() != null);

        EmployeeEntity savedEmployee = employeeDao.saveAndFlush(employeeEntity);
        cimEntity.setEmployee(savedEmployee);
        cimDao.saveAndFlush(cimEntity);
        return savedEmployee;
    }


}
