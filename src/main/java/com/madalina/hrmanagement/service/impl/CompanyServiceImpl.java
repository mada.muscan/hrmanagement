package com.madalina.hrmanagement.service.impl;

import com.madalina.hrmanagement.dao.CompanyDao;
import com.madalina.hrmanagement.entity.CompanyEntity;
import com.madalina.hrmanagement.service.CompanyService;
import com.madalina.hrmanagement.service.exception.NoEntityWithSuchIdException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyDao dao;

    public CompanyEntity save(CompanyEntity entity) {
        return dao.saveAndFlush(entity);
    }


    public CompanyEntity readById(Long id) {
        return dao.findById(id)
                .orElseThrow(() -> new NoEntityWithSuchIdException("No entity with such id"));
    }

    public List<CompanyEntity> readAll(Integer pageNumber, Integer pageSize) {
        return dao.findAll(PageRequest.of(pageNumber, pageSize)).toList();
    }

    public List<CompanyEntity> readAllCompanies() {
        return dao.findAll();
    }


    public void delete(Long id) {
        dao.deleteById(id);
    }


}
