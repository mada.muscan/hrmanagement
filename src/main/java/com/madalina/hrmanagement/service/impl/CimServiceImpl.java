package com.madalina.hrmanagement.service.impl;

import com.madalina.hrmanagement.dao.CimDao;
import com.madalina.hrmanagement.entity.CimEntity;
import com.madalina.hrmanagement.service.CimService;
import com.madalina.hrmanagement.service.exception.NoEntityWithSuchIdException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CimServiceImpl implements CimService {

    @Autowired
    private CimDao dao;

    @Override
    public CimEntity save(CimEntity entity) {
        return dao.saveAndFlush(entity);
    }

    @Override
    public CimEntity readById(Long id) {
        return dao.findById(id)
                .orElseThrow(() -> new NoEntityWithSuchIdException("No entity with such id"));
    }

    @Override
    public List<CimEntity> readAll(Integer pageNumber, Integer pageSize) {
        return dao.findAll(PageRequest.of(pageNumber, pageSize)).toList();
    }

    @Override
    public void delete(Long id) {
        dao.deleteById(id);
    }

    @Override
    public CimEntity readByEmployeeId(Long employeeId) {
        return dao.readFirstByEmployeeId(employeeId)
                .orElseThrow(() -> new NoEntityWithSuchIdException("No entity with such id"));

    }
}
