package com.madalina.hrmanagement.service.impl;

import com.madalina.hrmanagement.dao.MedicalCheckDao;
import com.madalina.hrmanagement.entity.MedicalCheckEntity;
import com.madalina.hrmanagement.service.MedicalCheckService;
import com.madalina.hrmanagement.service.exception.NoEntityWithSuchIdException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MedicalCheckServiceImpl implements MedicalCheckService {

    @Autowired
    private MedicalCheckDao dao;

    @Override
    public MedicalCheckEntity save(MedicalCheckEntity entity) {
        return dao.saveAndFlush(entity);
    }

    @Override
    public MedicalCheckEntity readById(Long id) {
        return dao.findById(id)
                .orElseThrow(() -> new NoEntityWithSuchIdException("No entity with such id"));
    }

    @Override
    public List<MedicalCheckEntity> readAll(Integer pageNumber, Integer pageSize) {
        return dao.findAll(PageRequest.of(pageNumber, pageSize)).toList();
    }

    @Override
    public void delete(Long id) {
        dao.deleteById(id);
    }
}
