package com.madalina.hrmanagement.service.impl;

import com.madalina.hrmanagement.dao.EventLogDao;
import com.madalina.hrmanagement.entity.EventLogEntity;
import com.madalina.hrmanagement.service.EventLogService;
import com.madalina.hrmanagement.service.exception.NoEntityWithSuchIdException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class EventLogServiceImpl implements EventLogService {

    @Autowired
    private EventLogDao dao;

    @Override
    public EventLogEntity save(EventLogEntity entity) {
        return dao.saveAndFlush(entity);
    }

    @Override
    public EventLogEntity readById(Long id) {
        return dao.findById(id)
                .orElseThrow(() -> new NoEntityWithSuchIdException("No entity with such id"));
    }

    @Override
    public List<EventLogEntity> readAll(Integer pageNumber, Integer pageSize) {
        return dao.findAll(PageRequest.of(pageNumber, pageSize)).toList();
    }

    @Override
    public void delete(Long id) {
        dao.deleteById(id);
    }
}
