package com.madalina.hrmanagement.service;

import com.madalina.hrmanagement.entity.CompanyEntity;

import java.util.List;

public interface CompanyService {
    CompanyEntity save(CompanyEntity entity);

    CompanyEntity readById(Long id);

    List<CompanyEntity> readAll(Integer pageNumber, Integer pageSize);
    List<CompanyEntity> readAllCompanies();

    void delete(Long id);
}
