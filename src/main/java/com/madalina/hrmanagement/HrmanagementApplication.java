package com.madalina.hrmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.madalina.hrmanagement."})
@SpringBootApplication
public class HrmanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(HrmanagementApplication.class, args);
	}

}
