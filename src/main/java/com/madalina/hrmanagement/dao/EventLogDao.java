package com.madalina.hrmanagement.dao;


import com.madalina.hrmanagement.entity.EventLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventLogDao extends JpaRepository<EventLogEntity, Long>{


}
