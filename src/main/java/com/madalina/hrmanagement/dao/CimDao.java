package com.madalina.hrmanagement.dao;


import com.madalina.hrmanagement.entity.CimEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CimDao extends JpaRepository<CimEntity, Long> {




}
