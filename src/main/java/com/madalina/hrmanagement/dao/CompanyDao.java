package com.madalina.hrmanagement.dao;


import com.madalina.hrmanagement.entity.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyDao extends JpaRepository<CompanyEntity, Long> {

}
