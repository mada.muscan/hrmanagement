package com.madalina.hrmanagement.dao;


import com.madalina.hrmanagement.entity.EmployeeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeDao extends JpaRepository<EmployeeEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM employees left join company_has_employees as che on employees.id = che.employees_id where che.company_id = :id")
    Integer countEmployeesByCompanyId(@Param("id") Long id);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM employees")
    Integer countEmployees();

    @Query(nativeQuery = true, value = "SELECT * FROM employees left join company_has_employees as che on employees.id = che.employees_id where che.company_id = :id")
    Page<EmployeeEntity> getEmployeesByCompanyId(@Param("id") Long id, Pageable pageable);


}
