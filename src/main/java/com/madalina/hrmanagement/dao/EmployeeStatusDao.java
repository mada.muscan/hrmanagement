package com.madalina.hrmanagement.dao;

import com.madalina.hrmanagement.entity.EmployeeStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeStatusDao extends JpaRepository<EmployeeStatusEntity, Long> {
}
