package com.madalina.hrmanagement.dao;


import com.madalina.hrmanagement.entity.EventTagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventTagDao extends JpaRepository<EventTagEntity, Long> {

}
