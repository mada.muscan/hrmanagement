package com.madalina.hrmanagement.dao;


import com.madalina.hrmanagement.entity.MedicalCheckEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MedicalCheckDao extends JpaRepository <MedicalCheckEntity, Long> {


}
