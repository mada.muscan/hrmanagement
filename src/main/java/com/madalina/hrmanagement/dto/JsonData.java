package com.madalina.hrmanagement.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JsonData {


    private String name = "";
    private Urls allCompaniesUrls = new Urls();
    private Urls oneCompanyUrls = new Urls();


}
