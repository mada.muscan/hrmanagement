package com.madalina.hrmanagement.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Urls {
    private String employeeAdd = "";
    private String employeeShow = "";
    private String eventAdd = "";
    private String eventShow = "";
    private String cimAdd = "";
    private String cimShow = "";
    private String medicalCheckAdd = "";
    private String medicalCheckShow = "";

}
