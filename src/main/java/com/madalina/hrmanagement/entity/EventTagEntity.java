package com.madalina.hrmanagement.entity;


import com.madalina.hrmanagement.utils.Tag;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "event_tag")
public class EventTagEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Tag tag;
}
