package com.madalina.hrmanagement.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "company")
public class CompanyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "companies", fetch = FetchType.LAZY)
    private List<EmployeeEntity> employees;


    @Override
    public int hashCode() {
        return this.id.intValue();
    }


    @Override
    public boolean equals(Object obj) {
        // check that input is null
        if (obj == null) {
            return false;
        }

        // check that input is the same data type as the current object
        if (!obj.getClass().equals(CompanyEntity.class)) {
            return false;
        }

        // check if the input is equal to the current object
        CompanyEntity input = (CompanyEntity) obj;
        if (input.getId().equals(this.getId())) {
            return true;
        } else {
            return false;
        }
    }
}

