package com.madalina.hrmanagement.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(name = "company_has_employees_isActive")
public class EmployeeStatusEntity {

    @Id
    private Long id;

    @Column(name="is_active")
    private boolean isActive;

    @ManyToOne
    @JoinColumn(name = "employees_id")
    private EmployeeEntity employeeEntity;


    @ManyToOne
    @JoinColumn(name = "company_id")
    private CompanyEntity companyEntity;

}
