package com.madalina.hrmanagement.entity;

import com.madalina.hrmanagement.utils.Tag;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@Entity
@Table(name = "event_log")
public class EventLogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String description;

    @Column(name = "date_event")
    private LocalDateTime dateEvent;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "event_log_has_event_tag",
            joinColumns = {@JoinColumn(name = "event_log_id")},
            inverseJoinColumns = {@JoinColumn(name = "event_tag_id")})
    private Set<EventTagEntity> tags;

    @ManyToOne
    @JoinColumn(name = "employees_id")
    private EmployeeEntity employee;

}
