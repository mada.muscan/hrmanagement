package com.madalina.hrmanagement.templateservices;


import com.madalina.hrmanagement.dto.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemplateService {

    @Autowired
    private JsonRepo jsonRepo;

    public String getAllCompaniesNavBar() {
        JsonData data = jsonRepo.getData();
        String result = jsonRepo.readTemplate("src/main/resources/static/html/navbar.html");

        result = result.replace("{{employeeAdd}}", data.getAllCompaniesUrls().getEmployeeAdd());
        result = result.replace("{{employeeShow}}", data.getAllCompaniesUrls().getEmployeeShow());
        result = result.replace("{{eventAdd}}", data.getAllCompaniesUrls().getEventAdd());
        result = result.replace("{{eventShow}}", data.getAllCompaniesUrls().getEventShow());
        result = result.replace("{{cimAdd}}", data.getAllCompaniesUrls().getCimAdd());
        result = result.replace("{{cimShow}}", data.getAllCompaniesUrls().getCimShow());
        result = result.replace("{{medicalCheckAdd}}", data.getAllCompaniesUrls().getMedicalCheckAdd());
        result = result.replace("{{medicalCheckShow}}", data.getAllCompaniesUrls().getMedicalCheckShow());

        return result;

    }

    public String getOneCompanyNavBar(Long companyId) {
        JsonData data = jsonRepo.getData();
        String result = jsonRepo.readTemplate("src/main/resources/static/html/navbar.html");

        result = result.replace("{{employeeAdd}}", data.getOneCompanyUrls().getEmployeeAdd().replace("{{companyId}}", companyId.toString()));
        result = result.replace("{{employeeShow}}", data.getOneCompanyUrls().getEmployeeShow().replace("{{companyId}}", companyId.toString()));
        result = result.replace("{{eventAdd}}", data.getOneCompanyUrls().getEventAdd().replace("{{companyId}}", companyId.toString()));
        result = result.replace("{{eventShow}}", data.getOneCompanyUrls().getEventShow().replace("{{companyId}}", companyId.toString()));
        result = result.replace("{{cimAdd}}", data.getOneCompanyUrls().getCimAdd().replace("{{companyId}}", companyId.toString()));
        result = result.replace("{{cimShow}}", data.getOneCompanyUrls().getCimShow().replace("{{companyId}}", companyId.toString()));
        result = result.replace("{{medicalCheckAdd}}", data.getOneCompanyUrls().getMedicalCheckAdd().replace("{{companyId}}", companyId.toString()));
        result = result.replace("{{medicalCheckShow}}", data.getOneCompanyUrls().getMedicalCheckShow().replace("{{companyId}}", companyId.toString()));

        return result;

    }


}
