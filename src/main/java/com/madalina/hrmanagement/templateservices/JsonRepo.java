package com.madalina.hrmanagement.templateservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.madalina.hrmanagement.dto.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class JsonRepo {

    @Autowired
    private ObjectMapper mapper;

    public JsonData getData()   {
        try {
            return mapper.readValue(new File("src/main/resources/static/JSON/vars.json"), JsonData.class);
        }
        catch (IOException exception){
            System.out.println("Could not open the following file: vars.json");
            return new JsonData();
        }
    }

    public String readTemplate(String path) {
        try {
            return Files.readString(Path.of(path));
        } catch (IOException exception) {
            System.out.println(String.format("Could not open the following file: %s", path));
            return "";
        }
    }
}