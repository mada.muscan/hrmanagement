package com.madalina.hrmanagement.controller;

import com.madalina.hrmanagement.entity.CompanyEntity;
import com.madalina.hrmanagement.entity.EmployeeEntity;
import com.madalina.hrmanagement.service.CompanyService;
import com.madalina.hrmanagement.service.EmployeeService;
import com.madalina.hrmanagement.templateservices.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private TemplateService templateService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private CompanyService companyService;

    @GetMapping("/{companyId}")
    public String viewCompany(@PathVariable("companyId") Long companyId, Model model) {
        CompanyEntity companyEntity = companyService.readById(companyId);
        model.addAttribute("companyName", companyEntity.getName());
        model.addAttribute("companyId", companyEntity.getId());
        model.addAttribute("URL", getUrl(companyId));
        model.addAttribute("navBar", templateService.getOneCompanyNavBar(companyId));
        return "company";
    }

    @GetMapping("/{companyId}/employees")
    public String showEmployeesByCompanyId(
            @PathVariable("companyId") Long companyId,
            @RequestParam(name = "pageNumber", defaultValue = "1") int pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "5") int pageSize,
            Model model) {
        List<EmployeeEntity> employeesByCompanyId = employeeService.getEmployeesByCompanyId(companyId, pageNumber - 1, pageSize);
        CompanyEntity companyEntity = companyService.readById(companyId);
        List<Integer> pages = employeeService.getPagesEmployeesByCompanyId(companyId, pageSize);
        model.addAttribute("employeeList", employeesByCompanyId);
        model.addAttribute("companyId", companyId);
        model.addAttribute("companyName", companyEntity.getName());
        model.addAttribute("pageNumber", pageNumber);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("slash", "/");
        model.addAttribute("pages", pages);
        model.addAttribute("URL", getUrl(companyId));
        model.addAttribute("lastPage", pages.size());
        model.addAttribute("navBar", templateService.getOneCompanyNavBar(companyId));
        return "showEmployees";
    }

    private String getUrl(Long companyId) {
        return "/company/" + companyId + "/employees";
    }

}
