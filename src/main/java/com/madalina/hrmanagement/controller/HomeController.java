package com.madalina.hrmanagement.controller;

import com.madalina.hrmanagement.entity.CompanyEntity;
import com.madalina.hrmanagement.service.CompanyService;
import com.madalina.hrmanagement.service.EmployeeService;
import com.madalina.hrmanagement.templateservices.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private TemplateService templateService;


    @Autowired
    private CompanyService companyService;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public String viewHomePage(Model model){
        List<CompanyEntity> companyList = companyService.readAllCompanies();

        Map<CompanyEntity, Integer> companyWithEmployeeCount = new HashMap<>();

        for (CompanyEntity company:companyList) {
            Integer count = employeeService.countEmployeesByCompanyId(company.getId());
            companyWithEmployeeCount.put(company, count);
        }

        /*for (Entry<CompanyEntity, Integer> entry: companyWithEmployeeCount.entrySet()) {}*/

        model.addAttribute("companyWithCount", companyWithEmployeeCount);
        model.addAttribute("navBar", templateService.getAllCompaniesNavBar());


        return "home";
    }
}
