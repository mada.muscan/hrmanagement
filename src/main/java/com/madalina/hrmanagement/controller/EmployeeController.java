package com.madalina.hrmanagement.controller;

import com.madalina.hrmanagement.dto.FormWrapper;
import com.madalina.hrmanagement.entity.CimEntity;
import com.madalina.hrmanagement.entity.CompanyEntity;
import com.madalina.hrmanagement.entity.EmployeeEntity;
import com.madalina.hrmanagement.service.CimService;
import com.madalina.hrmanagement.service.CompanyService;
import com.madalina.hrmanagement.service.EmployeeService;
import com.madalina.hrmanagement.templateservices.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private TemplateService templateService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CimService cimService;

    // http://localhost:8080/employee?pageNumber=0&pageSize=50

    @GetMapping("/all")
    public String showAllEmployees(
            @RequestParam(name = "pageNumber", defaultValue = "1") int pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "5") int pageSize,
            Model model) {
        List<EmployeeEntity> employeeList = employeeService.readAll(pageNumber - 1, pageSize);
        List<Integer> pages = employeeService.getPagesEmployees(pageSize);
        model.addAttribute("employeeList", employeeList);
        model.addAttribute("companyName", "All companies");
        model.addAttribute("pageNumber", pageNumber);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("empty", "");
        model.addAttribute("pages", pages);
        model.addAttribute("URL", getUrl());
        model.addAttribute("lastPage", pages.size());
        model.addAttribute("navBar", templateService.getAllCompaniesNavBar());

        return "showEmployees";
    }


    @GetMapping("/add")
    public String addEmployee(Model model) {
        List<CompanyEntity> allCompanies = companyService.readAllCompanies();
        model.addAttribute("allCompanies", allCompanies);
        model.addAttribute("formObject", new FormWrapper());
        model.addAttribute("navBar", templateService.getAllCompaniesNavBar());

        return "addEmployee";
    }

    @PostMapping(path = "/add")
    public RedirectView addEmployee(@ModelAttribute FormWrapper formObject) {
        System.out.println(formObject);
        employeeService.createEmployee(formObject);

        return new RedirectView("http://localhost:8080/home");
    }


    @GetMapping("/{employeeId}/edit")
    public String editEmployee(Model model, @PathVariable("employeeId") Long id) {

        CimEntity cimByEmployeeId = cimService.readByEmployeeId(id);
        EmployeeEntity employeeId = employeeService.readById(id);
        List<CompanyEntity> allCompanies = companyService.readAllCompanies();
        FormWrapper formWrapper = new FormWrapper();
        formWrapper.setInputFirstName(employeeId.getFirstName());
        formWrapper.setInputLastName(employeeId.getLastName());
        formWrapper.setInputOccupation(employeeId.getOccupation());
        formWrapper.setInputPhoneNumber(employeeId.getTelephone());
        formWrapper.setInputCim(cimByEmployeeId.getNumber());
        formWrapper.setInputCimStartDate(cimByEmployeeId.getStartDate().toLocalDate());
        formWrapper.setInputCimExpirationDate(cimByEmployeeId.getExpirationDate().toLocalDate());
        formWrapper.setIsActive(employeeId.getActive()?"Is active":null);
        formWrapper.setInputPhoneNumber(employeeId.getTelephone());


        model.addAttribute("allCompanies", allCompanies);
        model.addAttribute("formObject", formWrapper);
        model.addAttribute("navBar", templateService.getAllCompaniesNavBar());


        return "editEmployee";
    }

    @PostMapping(path = "/{employeeId}/edit")
    public RedirectView editEmployee(@ModelAttribute FormWrapper formObject) {
        System.out.println(formObject);
        employeeService.createEmployee(formObject);

        return new RedirectView("http://localhost:8080/home");
    }

    private String getUrl() {
        return "/employee/all";
    }

}
