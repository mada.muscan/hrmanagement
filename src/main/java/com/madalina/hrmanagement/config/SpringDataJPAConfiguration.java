package com.madalina.hrmanagement.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.madalina.hrmanagement.dao")
public class SpringDataJPAConfiguration {
}
